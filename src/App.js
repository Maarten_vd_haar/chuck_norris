import React, { Fragment, useState, useCallback, useEffect } from "react";
import { GlobalStyles, PageContainer, LogoWrapper, Logo, SubTitle, Button, ListContainer, List, Actions, Modal } from "./components";
import Chucky from "./chuck-norris.png";

const App = () => {
  const [errors, setErrors] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [modalTxt, setModalTxt] = useState('');
  const [jokes, setJokes] = useState([]);  
  const [favorites, setFavorites] = useState([]);
  const [loading, setLoading] = useState(false);  
  const storageData = JSON.parse(window.localStorage.getItem("favorites")) || [];
  const [isFetching, setIsFetching] = useState(false);

  const fetchRequest = useCallback(() => {
    fetch('http://api.icndb.com/jokes/random/10')
      .then(res => res.json())
      .then(data => { 
        if(data.type === 'success') {
          setJokes(data.value);
        } else {
          setLoading(true)
        }
      })
      .catch(err => setErrors(err))
  }, []);

  const addFavorite = (item) => {
    if(!favorites.includes(item) && favorites.length < 10) {
      setFavorites([item].concat(favorites));
      window.localStorage.setItem("favorites", JSON.stringify([item].concat(favorites)));
    } else if (favorites.includes(item)) {
      setModalTxt('Stop liking the same jokes over and over again... Thats no fun.');
    } else if (favorites.length == 10) {
      setModalTxt('Enough is enough, right?');
    }
  }

  const removeFavorite = (item) => {
    const newFavorites = favorites.filter(el => el.id !== item.id);
    setFavorites(newFavorites);
    
    if (newFavorites.length == 0 && !isFetching) {
        setIsFetching(false);
      setModalTxt("We've got rid of that pretty easily...");
      setShowModal(true);
    }

    window.localStorage.setItem("favorites", JSON.stringify(newFavorites));
  }

  const handleClick = useCallback(() => {
    setIsFetching(!isFetching)

    if(!isFetching) {
      fetchRandomJoke();
    }
  }); 

  const fetchRandomJoke = () => {
    if(favorites.length < 10) {
      fetch('http://api.icndb.com/jokes/random/1')
      .then(res => res.json())
      .then(data => {
        const item = data.value[0];

        if(!favorites.includes(data.value) && data.type == 'success') {
          setFavorites([item].concat(favorites));
          window.localStorage.setItem("favorites", JSON.stringify([item].concat(favorites)));
        } else if (favorites.includes(data.value)) {
          setModalTxt('We dont need that...');
        }
        
      })
      .catch(err => setErrors(err))
    } else {        
      setIsFetching(false);
      setModalTxt('Ok, too much jokes now!');
    }
  };

  useEffect(() => {
    if(isFetching) {
      const timer = setTimeout(() => {
          fetchRandomJoke();
      }, 1500);
      
      return () => {
        clearTimeout(timer);
      };
    }
  }, [isFetching, favorites])

  useEffect(() => {
    if(modalTxt !== '' && !showModal) {
      setShowModal(true);
    }
  }, [modalTxt]);

  useEffect(() => {
    if(storageData && storageData.length) {
      setFavorites(storageData);
    }
  }, []);

  return (
    <Fragment>
      <GlobalStyles />
      <PageContainer>
        <LogoWrapper>
          <SubTitle>Chuck</SubTitle>
          <Logo />
          <SubTitle>Norris</SubTitle>
        </LogoWrapper>
        <Actions>
          <Button disabled={loading} handleClick={fetchRequest}>Go get me some jokes (10)!</Button>
          <Button handleClick={handleClick} loading={!!isFetching}>Go get me one joke a time!</Button>
        </Actions>
        <ListContainer> 
          {jokes.length > 0 &&
            <List title="Jokes" list={jokes} favorites={favorites} handleClick={addFavorite} cta={"Add!"} />
          }
          {favorites.length > 0 && 
            <List title="Favorites" list={favorites} topTen handleClick={removeFavorite} cta={"Remove!"} />
          }
        </ListContainer>
      </PageContainer>
      {!!showModal && modalTxt !== '' &&
        <Modal handleClick={() => { setShowModal(!showModal), setModalTxt('')}} modalTxt={modalTxt}>
          <img src={Chucky} alt="Chucky" />
        </Modal>
      }
    </Fragment>
  );
};

export default App;