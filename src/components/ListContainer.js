import styled from 'styled-components';
import List from './List';

export const ListContainer = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin-top: 24px;

    ${List} + ${List} {
        margin-top: 24px;
    }  

    @media all and (min-width: 720px) {
        flex-direction: row;
        flex-wrap: wrap;

        ${List} {
            flex:1;  
            
            &:only-child {
                flex: 0 1 60%; 
            }

            + ${List} {
                margin: 0 0 0 4%;
            }
        }
    }
`;