import React from "react";
import styled, { css } from 'styled-components';
import Button from "./Button";
import { ListItem } from "./ListItem";

const List = ({ title, list, handleClick, cta, favorites, className, topTen }) => {
    return(
        <div className={className}>
            <h2>{title}{topTen && `(${list.length})`}</h2>
            <ul className={title}>
                {list.map(item => (
                    <ListItem key={item.id} selected={favorites && favorites.includes(item)}>
                        <p dangerouslySetInnerHTML={{
                        __html: item.joke
                        }} />
                        <Button handleClick={() => handleClick(item)}>{cta}</Button>
                    </ListItem>
                ))}
            </ul>
        </div>
    )
}

export default styled(List)`
    flex: 1;
    background: #fff;
    border: 1px solid #f2f2f2;
    border-radius: 5px;
    padding: 12px;
    color: #404040;

    p {
        padding-right: 12px;
    }

    ${props =>
        props.topTen &&
        css`
            li {
                position: relative;
                padding-left: 36px;

                &:before {
                    position: absolute;
                    left: 0;
                    counter-increment: counter;
                    content: counter(counter);
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    width: 20px;
                    height: 20px;
                    border: 1px solid #333;
                    border-radius: 50%;
                }
            }
        `
    }
`;