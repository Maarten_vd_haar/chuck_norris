import React from "react";
import styled from 'styled-components';
import { Button } from ".";

const Modal = ({ handleClick, modalTxt, children }) => {
    return(
        <ModalContainer>
          <ModalContent>
            <h1>{modalTxt}</h1>
            {children}
            <Button handleClick={handleClick}>Close it!</Button>
          </ModalContent>
        </ModalContainer>
    )
}

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 90vh;
  max-width: 50vw;
  min-width: 250px;
  min-height: 250px;
  background: #fff;
  border-radius: 5px;
  padding: 24px;
  margin: 24px;
  color: black;
  justify-content: space-between;
  align-items: center;

  img {
      max-width: 100%;
  }

  p {
      flex: 1;
  }
`;

const ModalContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1000;
    display: flex;
    justify-content: center;
    align-items: center;
    background: rgba(0,0,0,0.5);
`;

export default styled(Modal)``;
