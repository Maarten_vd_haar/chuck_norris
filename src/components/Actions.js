import styled from 'styled-components';
import Button from './Button';

export const Actions = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 24px;

  ${Button} {
    color: white;
  }
`;