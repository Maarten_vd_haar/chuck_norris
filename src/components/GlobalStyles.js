import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
    html, body {
        min-height: 100%;
    }

    body {
        background: url(https://chucknorris.com/assets/themes/chuck-norris/img/news-bg.png) #000 no-repeat bottom right;
        counter-reset: counter;
        color: #fff;
        font-family: "Inconsolata", monospace;
    }    

    ul {
        list-style-type: none;
        padding: 0;
    }

    p, h1, h2, ul {
        margin: 0;
    }
`;
