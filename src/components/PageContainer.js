import styled from 'styled-components';

export const PageContainer = styled.div`
    max-width: 1200px;
    padding: 0 12px;
    margin: 0 auto;
`;