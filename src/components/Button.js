import React from "react";
import styled, { css, keyframes } from 'styled-components';

const Button = ({ handleClick, children, className }) => {
    return(
        <button onClick={handleClick} className={className}>
            {children}
        </button>
    )
}

export default styled(Button)`
    border: 2px solid #FFC400;
    border-radius: 8px;
    background: transparent;
    display: inline-block;
    padding: 10px 6px 6px;
    font-weight: 700;
    font-size: 1,4rem;
    line-height: 1;
    text-transform: uppercase;
    margin: 0 6px;
    cursor: pointer;
    outline: none;
    position: relative;

    @media all and (min-width: 480px) {
        padding: 10px 30px 6px;
    }

    &:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        background: #FFC400;
        z-index: -1;
    }

    ${props =>
    props.loading &&
    css`
        &:before {
            animation: ${progress} 1.6s linear infinite;
        }
    `}

    &:hover {
        background: #FFC400;
        color: #404040;
    }
`;

const progress = keyframes`
  from {
    width: 0;
  }

  to {
    width: 100%
  }
`;