import styled, { css } from 'styled-components';

export const ListItem = styled.li`
    padding: 12px;
    border-top: 1px solid #333;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    background: ${props => (props.selected ? '#f2f2f2' : 'none')};
`;